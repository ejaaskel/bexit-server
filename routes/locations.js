var express = require('express');
var router = express.Router();



//THIS SHOULD BE THE FUNCTION TO GET THE NEARBY LOCATIONS AND CREATE NEW ONES
router.post('/getlocation', function(req, res) {
    var db = req.db;
    console.log(req.body);
    //check if location player has entered exists
    db.collection('locationlist').find({
    	$and: [
        { northwestLat: (parseFloat(req.body.northwestLat)).toFixed(6) },
        { northwestLon: (parseFloat(req.body.northwestLon)).toFixed(6) }
    	]
    }).toArray(function (err, items) {
	//if location does not exist create it
	if(items.length==0){
		//Infer the location type
		console.log("Getting type");
		var http = require('http')
		  , fs = require('fs')
		  , options

		options = {
			host: 'www.google.com'
		  , port: 80
		  , path: '/maps/api/staticmap?center='+req.body.northwestLat+','+req.body.nortwestLon+'&zoom=13&size=1x1&maptype=roadmap&key=AIzaSyAG10XtPkMcAcwMGauIktdiiIkt-fzUoSw'
		}
		console.log("SStarting request");
		//Download one pixel sized image where location type will be infered
		var request = http.get(options, function(resInner){
				var imagedata = ''
				resInner.setEncoding('binary')

				resInner.on('data', function(chunk){
					console.log('data');
					imagedata += chunk
				})

				resInner.on('end', function(){
					console.log("END");
					fs.writeFile('google9999.png', imagedata, 'binary', function(err){
						if (err) throw err
						console.log('File saved.')
					})
					
					var PNG = require('png-js');
					PNG.decode('google9999.png', function(pixels) {
						// pixels is a 1d array (in rgba order) of decoded pixel data
						console.log(pixels[0]);
						console.log(pixels[1]);
						console.log(pixels[2]);
						var locationType;
						if(pixels[0]==210 && pixels[1]==228&&pixels[2]==200){
							console.log("In forest!");
							locationType = "forest"
						}
						else if(pixels[0]==202 && pixels[1]==223&&pixels[2]==170){
							console.log("In another forest!");
							locationType = "forest"
						}
						else if(pixels[0]==232 && pixels[1]==221&&pixels[2]==189){
							console.log("In building!");
							locationType = "building"
						}
						else if(pixels[0]==227 && pixels[1]==227&&pixels[2]==227){
							console.log("In parking lot!");
							locationType = "parkinglot"
						}
						else if(pixels[0]==235 && pixels[1]==210&&pixels[2]==207){
							console.log("In hospital");
							locationType = "hospital"
						}
						else if(pixels[0] < 177 && pixels[0]>153 && pixels[1]<207&& pixels[1]>190&&pixels[2]<=254&&pixels[2]>=253){
							console.log("In water!");
							locationType = "water"
						}
						else {
							console.log("Sp,ewhere else");
							locationType = "otherPlace"
						}	
					    //create the location
						db.collection('locationlist').insert(
						{
								northwestLat:(parseFloat(req.body.northwestLat)).toFixed(6),
								northwestLon:(parseFloat(req.body.northwestLon)).toFixed(6),
								southeastLat:(parseFloat(req.body.northwestLat)+0.001).toFixed(6),
								southeastLon:(parseFloat(req.body.northwestLon)+0.001).toFixed(6),
								//owner:req.body.ownerID,
								typeOfLocation: locationType,
								timesConquered:1}
						,function(err, result){
							//Find nearby locations and send them
							db.collection('locationlist').find({
							$and:[
								{northwestLat: { $gt: (parseFloat(req.body.exactNorthwestLat)-0.01).toFixed(6), 
														 $lt: (parseFloat(req.body.exactNorthwestLat)+0.01).toFixed(6) }},
								{northwestLon: { $gt: (parseFloat(req.body.exactNorthwestLon)-0.01).toFixed(6), 
										 $lt: (parseFloat(req.body.exactNorthwestLon)+0.01).toFixed(6) }}
							]
							}).toArray(function (err, items) {
							res.json(items);
							});
					  });
					});
				})
			})
			console.log("end");
        }
	    //if location already exists
        else{
            //Find nearby locations and send them
	    db.collection('locationlist').find({
		$and:[
			{northwestLat: { $gt: (parseFloat(req.body.exactNorthwestLat)-0.01).toFixed(6), 
                                         $lt: (parseFloat(req.body.exactNorthwestLat)+0.01).toFixed(6) }},
			{northwestLon: { $gt: (parseFloat(req.body.exactNorthwestLon)-0.01).toFixed(6), 
					 $lt: (parseFloat(req.body.exactNorthwestLon)+0.01).toFixed(6) }}
		]
	    }).toArray(function (err, items) {
		res.json(items);
	    });

           	
        }
    });
});


/* Update location with put */
//THIS SHOULD BE USED FOR CONQUERING AN EXISTING LOCATION
router.put('/location/:id', function(req, res, next) {
    var db = req.db
    var locationToUpdate = req.params.id;
    var doc = { $set: req.body};
    console.log(userToUpdate);
    console.log(doc);
    //Update location with id with parameters that are given in body
    db.collection('locationlist').updateById(locationToUpdate, doc ,function(err, result) {
        console.log(err);
        
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );

    });
});

//Loot location
//Id is not really required in the url
router.put('/location/loot/:id', function(req, res, next) {
    var db = req.db
    var locationToUpdate = req.params.id;
    var doc = { $set: req.body};
    var db = req.db;
    console.log(req.body);
    //check if location player has entered exists
    db.collection('locationlist').find({
    	$and: [
        { northwestLat: (parseFloat(req.body.northwestLat)).toFixed(6) },
        { northwestLon: (parseFloat(req.body.northwestLon)).toFixed(6) }
    	]
    }).toArray(function (err, items) {
	//if location does not exist
		if(items.length==0){
			console.log("invalid location");
		    var reply = {};
		    reply['error'] = 'invalid location';
		    res.json(reply);
		}
		else{
			db.collection('userlist').find({'username':req.body.username}).toArray(function (err, items) {
			//Check if user is valid
			if(items.length == 0){
				console.log("invalid user");
				var reply = {};
				reply['error'] = 'invalid user';
				res.json(reply);
			}
			else{
				//if player and user exists, remove some soldiers and add gold
				var playerStats = items[0];
				playerStats["soldiers"] = playerStats["soldiers"] - 10;
				if(playerStats["soldiers"]< 0){
					console.log("not neough soldiers");
					var reply = {};
					reply['error'] = 'not enough soldiers';
					res.json(reply);					
				}
				playerStats["money"] = parseInt(playerStats["money"]) + 50;
				//reply with new values
									db.collection('userlist').update( {'username':req.body.username}, 
									  {$set:{money:playerStats["money"],
										   soldiers:playerStats["soldiers"]}}, 
									  function(err,result){    
										  console.log(result);
										  res.send(
										  (err === null) ? { msg: '', lootedgold: 50, lostsoldiers:10 } : { msg: err })

									  });
			}
		});
	}
	});
});

//Get list of all locations
router.get('/', function(req, res) {
    var db = req.db;
    db.collection('locationlist').find().toArray(function (err, items) {
        res.json(items);
    });
});

//post new location to locationlist. Most likely not needed
router.post('/', function(req, res) {
    var db = req.db;
    db.collection('locationlist').insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

module.exports = router;
