var express = require('express');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;

//Get all users
router.get('/userlist', function(req, res) {
    var db = req.db;
    db.collection('userlist').find().toArray(function (err, items) {
        res.json(items);
    });
});

//Add new user
router.post('/userlist', function(req, res) {
    var db = req.db;
    db.collection('userlist').insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

//Login user
router.post('/login', function(req, res) {
    var db = req.db;
    db.collection('userlist').find({'username':req.body.username,'password':req.body.password}).toArray(function (err, items) {
        console.log(items);
		//if user not found reply with error
		if(items.length == 0){
			var reply = {};
			reply['error'] = 1;
			res.json(reply);
		}
		//else reply user data
		else{
			var reply = {};
			reply['error'] = 0;
			reply['id'] = items[0]['_id']
            reply['northwestLat'] = items[0]['northwestLat']
            reply['northwestLon'] = items[0]['northwestLon']
            reply['soldiers'] = parseInt(items[0]['soldiers'])
            reply['money'] = parseInt(items[0]['money'])
			res.json(reply);		
		}
		
    });
});

//Establish a base for user
router.post('/base/:id', function(req,res) {
    var db = req.db;
    var userToUpdate = req.params.id;
    console.log(req.body);
    db.collection('userlist').find({'username':req.body.username}).toArray(function (err, items) {
        console.log(items);
        //Check if user is valid
		if(items.length == 0){
			console.log("invalid user");
			var reply = {};
			reply['error'] = 'invalid user';
			res.json(reply);
	}
	//Check if location is in database
	else{
	    db.collection('locationlist').find({
    	        $and: [
                    { northwestLat: (parseFloat(req.body.northwestLat)).toFixed(6) },
                    { northwestLon: (parseFloat(req.body.northwestLon)).toFixed(6) }
    	        ]
            }).toArray(function (err, items) {		
				if(items.length==0){
					console.log("invalid location");
					var reply = {};
					reply['error'] = 'invalid location';
					res.json(reply);
				}
				//Add base coordinates to database	
				else{
					console.log("Addinf");
					db.collection('userlist').update( {'username':req.body.username}, 
									  {$set:{northwestLat:(parseFloat(req.body.northwestLat)).toFixed(6),
										   northwestLon:(parseFloat(req.body.northwestLon)).toFixed(6),
										   southeastLat:(parseFloat(req.body.northwestLat)+0.001).toFixed(6),
										   southeastLon:(parseFloat(req.body.northwestLon)+0.001).toFixed(6)}}, 
									  function(err,result){    
										  console.log(result);
										  res.send(
										  (err === null) ? { msg: '' } : { msg: err })

									  });
				}      
			});
        }
    });
});



//Delete a base for user
router.delete('/base/:id', function(req,res) {
    var db = req.db;
    var userToUpdate = req.params.id;
    console.log(req.body);
    db.collection('userlist').find({_id:new ObjectID(req.params.id)}).toArray(function (err, items) {
		if(items.length==0){
			console.log("invalid location");
		    var reply = {};
		    reply['error'] = 'invalid location';
		    res.json(reply);
		}
		else{
			db.collection('userlist').update( {_id:new ObjectID(req.params.id)}, 
						{$unset:{northwestLat:"",
	      				northwestLon:"",
	      				southeastLat:"",
	      				southeastLon:""}}, 
						function(err,result){    
							      console.log(result);
							      res.send((err === null) ? { msg: '' } : { msg: err })
			});
		}
	});
});


module.exports = router;
